
/*
***********************************************
CC(2019)
Author: Quarchodron
Creator client side main file.
***********************************************
*/

enableEvent_Render(true); // optional if is not alredy on

GUICreator <- {
	isOpen = false,
	
	// Packets var
	packetReadString = 290,
	packetString = 291,
	packedEndString = 292,

	packetReadTemplate = 293,
	packetTemplate = 294,
	packetEndTemplate = 295,

	Controllers = {}, // Controllers
	Elements = [], // Elements (draws, textures etc)
	Template = [], // Template backup
	
	Line = [],

	// Active element var
	activeElement = null,

	// Default textures in pack.
	texturesSelect = [
		"Inv_Slot_Equipped_Highlighted.tga",
		"Inv_Slot_Equipped.TGA",        
		"Inv_Slot_Focus.TGA",
		"Inv_Slot_Highlighted.TGA",
		"Inv_Slot_Highlighted_Focus.TGA",
		"MENU_INGAME.TGA",
		"BAR_BACK.TGA",
		"BAR_HEALTH.TGA",
		"Dlg_Conversation.TGA",
		"Map_NewWorld.TGA",
		"SELECT.TGA",
		"ARROW_DOWN_ELM.TGA",
		"ARROW_UP_ELM.TGA",
		"ARROW_LEFT_ELM.TGA",
		"ARROW_RIGHT_ELM.TGA",
		"BUTTON_ELM.TGA",
		"ROUND_ELM.TGA",
		"CHECKBOX_CHECKED_ELM.TGA",
		"CHECKBOX_ELM.TGA",
		"FRAME_ELM.TGA",
		"SLIDER_ELM.TGA",
		"LABEL_ELM.TGA",
		"WINDOW_1_1_ELM.TGA",
		"LOGO_ELM.TGA",
		"WINDOW_16_9_ELM.TGA",
		"WINDOW_4_3_ELM.TGA",
		"WINDOW_5_4_ELM.TGA",
		"DLG_CONVERSATION.TGA",
		"MENU_INGAME.TGA",
		"INV_SLOT_FOCUS.TGA",
		"INV_SLOT_HIGHLIGHTED_FOCUS.TGA",
		"PROGRESS.TGA",
	],

	// Default draws in pack.
	drawSelect = [
		"FONT_DEFAULT.TGA",
		"Font_20_Book_Hi.TGA",
		"Font_Old_10_White.TGA",
		"Font_Old_20_White.TGA",
		"Font_10_Book_Hi.TGA",
	],
};

/*
***************************************
Open creator function
***************************************
*/
function GUICreator::open()
{
	GUICreator.Controllers.init.show();

	isOpen = true;

	setFreeze(true);
	disableControls(true);
	showPlayerStatus(false);
	Camera.enableMovement(false);
	setCursorVisible(true);
	enableKeys(false);
}

/*
***************************************
Close creator function
***************************************
*/
function GUICreator::close()
{
	foreach(v,k in GUICreator.Controllers)
	{
		k.hide();
	}

	isOpen = false;

	GUICreator.Controllers.clear();
	GUICreator.Elements.clear();
	GUICreator.activeElement = null;
	GUICreator.Line.clear();
	
	setFreeze(false);
	disableControls(false);
	showPlayerStatus(true);
	Camera.enableMovement(true);
	setCursorVisible(false);
	enableKeys(true);
}

/*
***************************************
Add new element to array Elements
 Params:
 -- type - value (int)
***************************************
*/
function GUICreator::addNewElement(type)
{
	if(type == 1) { // Draw
		GUICreator.Elements.append(ElementDraw());
		GUICreator.Elements[GUICreator.Elements.len()-1].show();
	}else if(type == 2){ // Texture
		GUICreator.Elements.append(ElementTexture());
		GUICreator.Elements[GUICreator.Elements.len()-1].show();
	}
	GUICreator.activeElement = GUICreator.Elements.len()-1;

	reloadOperationsList();
	reloadElementPreview();
}

/*
***************************************
Remove element from array Elements
 Params:
 -- id - value (int)
***************************************
*/
function GUICreator::deleteElement(id)
{
	GUICreator.Elements.remove(id);

	if(GUICreator.Elements.len() > 0)
	{
		foreach(v,k in GUICreator.Elements)
		{
			GUICreator.activeElement = v;
		}
	}else{
		GUICreator.activeElement = null;
	}

	reloadOperationsList();
	reloadElementPreview();
}

/*
***************************************
Save project in form we can take as resault
***************************************
*/
function GUICreator::saveProject()
{
	// send packet as information we wanna save our project.
	local packet = Packet();
	packet.writeUInt16(GUICreator.packetReadString);
	packet.send(RELIABLE_ORDERED);

	// sort clone of table
	local operationTab = clone GUICreator.Elements;
	operationTab.sort(sortGuiCreatorArray);

	// get All elements
	foreach(value in operationTab)
	{
		// take lines to save from element (as array)
		foreach(val in value.getLinesToSave())
		{
			local packet = Packet();
			packet.writeUInt16(GUICreator.packetString);
			packet.writeString(val);
			packet.send(RELIABLE_ORDERED);
		}
	}
	// send packet as information we wanna end saving our project
	local packet = Packet();
	packet.writeUInt16(GUICreator.packedEndString);
	packet.send(RELIABLE_ORDERED);

	// save template
	saveTemplate();
}

/*
***************************************
Save project in form we can take as template
***************************************
*/
function GUICreator::saveTemplate()
{
	// send packet as information we wanna save our project.
	local packet = Packet();
	packet.writeUInt16(GUICreator.packetReadTemplate);
	packet.send(RELIABLE_ORDERED);

	// sort clone of table
	local operationTab = clone GUICreator.Elements;
	operationTab.sort(sortGuiCreatorArray);

	// get All elements
	foreach(value in operationTab)
	{
		// take lines to save from element (as string)
		local packet = Packet();
		packet.writeUInt16(GUICreator.packetTemplate);
		packet.writeString(value.getLinesToTemplate());
		packet.send(RELIABLE_ORDERED);
	}

	// send packet as information we wanna end saving our project
	local packet = Packet();
	packet.writeUInt16(GUICreator.packetEndTemplate);
	packet.send(RELIABLE_ORDERED);
}

/*
***************************************
Open project from template we load
***************************************
*/
function GUICreator::openProject()
{
	// If we have no elements and template is not empty
	if(Elements.len() == 0 && Template.len() > 0)
	{
		// Recognize element from Template as is it Draw or Texture
		foreach(temp in Template)
		{
			if(ElementTexture.checkISItTextureTemplate(temp)){
				Elements.append(ElementTexture());
				Elements[Elements.len()-1].show();
				Elements[Elements.len()-1].loadTemplate(temp);
			}else if(ElementDraw.checkISItDrawTemplate(temp)){
				Elements.append(ElementDraw());
				Elements[Elements.len()-1].show();
				Elements[Elements.len()-1].loadTemplate(temp);
			}
			continue;
		}
	// Active element as last of loaded
	activeElement = Elements.len()-1;

	reloadOperationsList();
	reloadElementPreview();
	}
}

/*
***************************************
Copy current element and creates new
***************************************
*/
function GUICreator::CopyCurrentElement(type)
{
	local currentElement = Elements[activeElement];
	local currPos = currentElement.getPosition();
	local currAlpha = currentElement.getAlpha();
	local currColor = currentElement.getColor();
	local newElement;

	if(type == "DRAW") {
		 newElement = ElementDraw();
		 newElement.setFont(currentElement.getFont());
	}else{
		 newElement = ElementTexture();
		 local size = currentElement.getSize();
		 local rect = currentElement.getRect();
		 if(currentElement.changedRect){
			 newElement.setRect(rect.x, rect.y, rect.width, rect.height);
 		 }
		 newElement.setSize(size.width, size.height);
		 newElement.setTexture(currentElement.getTexture());
	}

 	newElement.setPosition(currPos.x, currPos.y);
	newElement.setColor(currColor.r,currColor.g, currColor.b);
	newElement.setAlpha(currAlpha);

	Elements.append(newElement);
	Elements[Elements.len()-1].show();
	activeElement = Elements.len()-1;

	reloadOperationsList();
	reloadElementPreview();
}

/*
***************************************
Slide up current element
***************************************
*/
function GUICreator::SlideUpCurrentElement()
{
	local biggestTop = 0;
	foreach(v,k in Elements){
		if(k.topPosition > biggestTop) biggestTop = k.topPosition;
	}
	biggestTop = biggestTop + 1;
	Elements[activeElement].top();
	Elements[activeElement].topPosition = biggestTop;
	Elements.sort(sortGuiCreatorArray);
	reloadOperationsList();
	activeElement = Elements.len()-1;
}

function GUICreator::DrawLineOutsideElement(element)
{
	Line.clear();
	
	local drawXY = element.element.getPosition();
	local drawWH = element.getSize();
	local line = Line2d(drawXY.x, drawXY.y, drawXY.x, drawXY.y + drawWH.height);line.visible = true;line.setColor(255,0,0);Line.append(line);
	local line = Line2d(drawXY.x, drawXY.y + drawWH.height, drawXY.x + drawWH.width, drawXY.y + drawWH.height);line.visible = true;line.setColor(255,0,0);Line.append(line);		
	local line = Line2d(drawXY.x, drawXY.y, drawXY.x + drawWH.width, drawXY.y);line.visible = true;line.setColor(255,0,0);Line.append(line);		
	local line = Line2d(drawXY.x +drawWH.width, drawXY.y, drawXY.x + drawWH.width, drawXY.y + drawWH.height);line.visible = true;line.setColor(255,0,0);Line.append(line);		
}

function GUICreator::HideLineOutsides()
{
	Line.clear();
}

/*
***************************************
Packet handler with we can load our template
***************************************
*/
function GUICreator::packetHandler(packet)
{
	local id = packet.readUInt16();

	switch(id)
	{
		case GUICreator.packetTemplate:
			local val = packet.readString();
			GUICreator.Template.append(val);
		break;
	}
}

addEventHandler("onPacket", GUICreator.packetHandler);

/// OTHER FUNCTIONS

function sortGuiCreatorArray(first, second) {
  if (first.topPosition > second.topPosition) return 1;
  if (first.topPosition < second.topPosition) return -1;
  return 0;
}

function boolToInt(bool)
{
	if(bool) return 1;

	return 0;
}

function intToBool(int)
{
	if(int == 1) return true;

	return false;
}


/*
***************************************
Handlers for events we need in creator.
***************************************
*/

local function clickMouseHandler(btn)
{
	if(GUICreator.activeElement != null)
	{
		GUICreator.Elements[GUICreator.activeElement].clickEvent(btn);
	}
}
addEventHandler("onMouseClick", clickMouseHandler);

// get from old gui-framework by Tommy
local limit = 0;
local waitFPS = 0;
local frame = 0;
//////////////////////////////////////
local function renderHandler()
{
    waitFPS = getFpsRate().tofloat()/28.0;
    if(frame > limit)
    {
        limit = frame + waitFPS;
        if(GUICreator.activeElement != null)
		{
			GUICreator.Elements[GUICreator.activeElement].fpsEvent();
		}
    }
    ++frame;
}

addEventHandler("onRender", renderHandler);

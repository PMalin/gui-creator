
/**
Author: Quarchodron (http://gothic-online.com.pl/forum/member.php?action=profile&uid=15)
CC ( 2019 )
**/

class Element
{
	element = null;
	changedColor = false;
	changedAlpha = false;
	topPosition = 0;
	
	constructor()
	{
		changedColor = false;
		changedAlpha = false;
		element = null;
		topPosition = GUICreator.Elements.len();
	}
	
	function show()
	{
		element.visible = true;
	}
	
	function hide()
	{
		element.visible = false;
	}
	
	function top()
	{
		element.top();
	}
	
	function setPosition(x,y)
	{
		if(x == "" || x == null) x = 0;
		if(y == "" || y == null) y = 0;
		element.setPosition(x,y);
	}	
	
	function followCursor()
	{
		local cur = getCursorPosition();
		element.setPosition(cur.x + operandMove.posx, cur.y + operandMove.posy);
	}
	
	function getPosition()
	{
		return element.getPosition();
	}
	
	function getAlpha()
	{
		return element.getAlpha();
	}
	
	function setAlpha(val)
	{
		if(val >= 255){
			changedAlpha = false;
			element.setAlpha(255);
			return;
		}
		changedAlpha = true;
		element.setAlpha(val);
	}
	
	function getColor()
	{
		return element.getColor();
	}
	
	function setColor(r,g,b)
	{
		if(r == "" || r == null) r = 0;
		if(g == "" || g == null) g = 0;
		if(b == "" || b == null) b = 0;		
		changedColor = true;
		
		element.setColor(r,g,b);
	}
	
	function getHeightExtendedGui()
	{
		return getHeightGuiState();
	}
	
	function hidePreparedGui()
	{
		hideCurrentGuiState();
	}	
	
	function destroyPreparedGui()
	{
		hideCurrentGuiState();
	}
}
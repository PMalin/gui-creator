
/**
Author: Quarchodron (http://gothic-online.com.pl/forum/member.php?action=profile&uid=15)
CC ( 2019 )
**/
class ElementDraw extends Element
{
	operandMove = null;
	changedFont = false;
	infoPing = false;
	pingStats = null;
	
	constructor()
	{
		changedFont = false;
		base.constructor();
		operandMove = { active =false, posx = 0, posy = 0 };	
		element = Draw(3500,3500,"Przykładowy Draw");
		infoPing = false;
		pingStats = {x = 0, y = 0};		
	}	
	
	function ping(initialize = true)
	{
		if(initialize){
			local pos = element.getPosition();
			infoPing = 50;
			pingStats = {x = pos.x, y = pos.y};
			element.setPosition(x,y);
		}
		
		if(infoPing != false)
		{
			element.setPosition(pingStats.x - 50 + rand() % 100,pingStats.y - 50 + rand() % 100);
			infoPing = infoPing - 1;
			if(infoPing < 0){
				element.setPosition(pingStats.x, pingStats.y);
				infoPing = false;
			}
		}
	}
		
	
	function getSize()
	{
		return {width = element.width, height = element.height};
	}
	
	function getFont()
	{
		return element.font;
	}
	
	function setFont(val)
	{
		changedFont = true;
		element.font = val;
	}
	
	function getText()
	{
		return element.text;
	}
	
	function setText(val)
	{
		element.text = val;
	}
		
	function getInfo()
	{
		return "Draw";
	}
	
	function fpsEvent()
	{
		if(element.visible)
		{
			if(operandMove.active)
			{
				if(isMouseBtnPressed(MOUSE_LMB))
				{
					this.followCursor();
					reloadGUIState();
				}else{
					operandMove.active = false;
				}
			}
		}
		ping(false);
	}
	
	function clickEvent(btn)
	{
		if(element.visible)
		{
			if(btn == MOUSE_LMB)
			{
				if(ControllerInit.isMouseOnDraw(element))
				{
					local cur = getCursorPosition();
					local pos = element.getPosition();
					operandMove.posx = pos.x - cur.x;
					operandMove.posy = pos.y - cur.y;
					operandMove.active = true;
				}
			}
		}		
	}		
	
	function extendedGuiPrepare(x,y)
	{	
		setActiveType("DRAW");
		showCurrentGuiState(x,y,"DRAW");
	}
	
	function getLinesToSave()
	{
		local retArra = [];
		local pos = element.getPosition();
		local posExtend = element.getPositionPx();
		local color = element.getColor();
		retArra.append("local draw = Draw("+pos.x+","+pos.y+",\""+element.text+"\");");
		if(changedFont == true){
			retArra.append("draw.font=\""+element.font+"\"");
		}	
		if(changedColor == true){
			retArra.append("draw.setColor("+color.r+","+color.g+","+color.b+");");
		}	
		if(changedAlpha == true){
			retArra.append("draw.setAlpha("+element.getAlpha()+");");
		}
		retArra.append("//draw.setPositionPx("+posExtend.x+","+posExtend.y+");");		
		return retArra;
	}	
	
	function getLinesToTemplate()
	{
		local pos = element.getPosition();
		local color = element.getColor();
		return pos.x+" "+pos.y +" "+color.r+" "+color.g+" "+color.b+ " " + boolToInt(changedFont) + " " + element.font+" " + boolToInt(changedAlpha) + " " + element.getAlpha()+ " " + element.text;
	}
	
	static function checkISItDrawTemplate(template)
	{
		local arg = sscanf("ddddddsdds", template);
		if(arg) { 
			return true;
		}
		return false;
	}
	
	function loadTemplate(template)
	{
		local arg = sscanf("ddddddsdds", template);
		if(arg) { 
			element.setPosition(arg[0], arg[1]);
			element.setColor(arg[2], arg[3], arg[4]);
			if(arg[5] == 1){
				changedFont = true;
				element.font = arg[6];
			}
			if(arg[7] == 1){
				changedAlpha = true;
				element.setAlpha(arg[8]);
			}
			element.text= arg[9];
		}			
	}
}

/**
Author: Quarchodron (http://gothic-online.com.pl/forum/member.php?action=profile&uid=15)
CC ( 2019 )
**/

local activeType = 0;
/////////////////////////////////////////////////////////
local drawOperate = Draw(0,0, "Tekstura");
local textureOperate = Texture(0,0,2000,200,"SELECT.TGA");textureOperate.setAlpha(150);textureOperate.setColor(0,0,0);
local activeButtonOperator = false;
local selectedButtonOperator = false;
local selectTextureOperator = [];
local lastLoadedPosition = {x = 0, y = 0};

/////////////////////////////////////////////////////////

local otherButtons = {
	visual = {
		buttonCopy = Texture(0,0,800,200,"SELECT.TGA"),
		drawCopy = Draw(0,0,"Kopiuj"),
		buttonTop = Texture(0,0,800,200,"SELECT.TGA"),
		drawTop = Draw(0,0,"Przesu�"),
	},
};

/////////////////////////////////////////////////////////

local colorObject = {
	visual = {
		draw = Draw(0,0, "Kolor"),
		selectionTextureR = Texture(0,0,400,200,"SELECT.TGA"),
		selectionDrawR = Draw(0,0,""),
		selectionTextureG = Texture(0,0,400,200,"SELECT.TGA"),
		selectionDrawG = Draw(0,0,""),
		selectionTextureB = Texture(0,0,400,200,"SELECT.TGA"),
		selectionDrawB = Draw(0,0,""),
	},
	activeR = false,
	activeG = false,
	activeB = false,
};

colorObject.visual.selectionTextureR.setColor(0,0,0); colorObject.visual.selectionTextureR.setAlpha(150);
colorObject.visual.selectionTextureG.setColor(0,0,0); colorObject.visual.selectionTextureG.setAlpha(150);
colorObject.visual.selectionTextureB.setColor(0,0,0); colorObject.visual.selectionTextureB.setAlpha(150);

/////////////////////////////////////////////////////////

local positionObject = {
	visual = {
		draw = Draw(0,0, "Pozycja"),
		selectionTextureX = Texture(0,0,400,200,"SELECT.TGA"),
		selectionDrawX = Draw(0,0,""),
		selectionTextureY = Texture(0,0,400,200,"SELECT.TGA"),
		selectionDrawY = Draw(0,0,""),
	},
	activeX = false,
	activeY = false,
};

positionObject.visual.selectionTextureX.setColor(0,0,0); positionObject.visual.selectionTextureX.setAlpha(150);
positionObject.visual.selectionTextureY.setColor(0,0,0); positionObject.visual.selectionTextureY.setAlpha(150);

/////////////////////////////////////////////////////////

local sizeObject = {
	visual = {
		draw = Draw(0,0, "Wielko��"),
		selectionTextureX = Texture(0,0,400,200,"SELECT.TGA"),
		selectionDrawX = Draw(0,0,""),
		selectionTextureY = Texture(0,0,400,200,"SELECT.TGA"),
		selectionDrawY = Draw(0,0,""),
	},
	activeX = false,
	activeY = false,
};

sizeObject.visual.selectionTextureX.setColor(0,0,0); sizeObject.visual.selectionTextureX.setAlpha(150);
sizeObject.visual.selectionTextureY.setColor(0,0,0); sizeObject.visual.selectionTextureY.setAlpha(150);

/////////////////////////////////////////////////////////

local alphaObject = {
	visual = {
		draw = Draw(0,0, "Alpha"),
		selectionTexture = Texture(0,0,400,200,"SELECT.TGA"),
		selectionDraw = Draw(0,0,""),
	},
	active = false,
};

alphaObject.visual.selectionTexture.setColor(0,0,0); alphaObject.visual.selectionTexture.setAlpha(150);


/////////////////////////////////////////////////////////

local textObject = {
	visual = {
		draw = Draw(0,0, "Tekst:"),
		selectionTexture = Texture(0,0,1400,200,"SELECT.TGA"),
		selectionDraw = Draw(0,0,"Przyk�adowy draw"),
	},
	active = false,
};

textObject.visual.selectionTexture.setColor(0,0,0); textObject.visual.selectionTexture.setAlpha(150);

/////////////////////////////////////////////////////////

local rectObject = {
	visual = {
		draw = Draw(0,0, "Rect"),
		selectionTextureX = Texture(0,0,400,200,"SELECT.TGA"),
		selectionDrawX = Draw(0,0,""),
		selectionTextureY = Texture(0,0,400,200,"SELECT.TGA"),
		selectionDrawY = Draw(0,0,""),
		selectionTextureWIDTH = Texture(0,0,400,200,"SELECT.TGA"),
		selectionDrawWIDTH = Draw(0,0,""),
		selectionTextureHEIGHT = Texture(0,0,400,200,"SELECT.TGA"),
		selectionDrawHEIGHT = Draw(0,0,""),
	},
	activeX = false,
	activeY = false,
	activeWIDTH = false,
	activeHEIGHT = false,
};

rectObject.visual.selectionTextureX.setColor(0,0,0); rectObject.visual.selectionTextureX.setAlpha(150);
rectObject.visual.selectionTextureY.setColor(0,0,0); rectObject.visual.selectionTextureY.setAlpha(150);
rectObject.visual.selectionTextureWIDTH.setColor(0,0,0); rectObject.visual.selectionTextureWIDTH.setAlpha(150);
rectObject.visual.selectionTextureHEIGHT.setColor(0,0,0); rectObject.visual.selectionTextureHEIGHT.setAlpha(150);

/////////////////////////////////////////////////////////

function showCurrentGuiState(x,y,type)
{
	if(type == null) { return; }

	lastLoadedPosition.x = x;
	lastLoadedPosition.y = y;
	activeType = type;
	/////////////////////////////////////////////////////
	local posObj = GUICreator.Elements[GUICreator.activeElement].getPosition();

	positionObject.visual.draw.setPosition(x+50,y+70);
	positionObject.visual.selectionTextureX.setPosition((x+50) + positionObject.visual.draw.width + 50, y);positionObject.visual.selectionDrawX.setPosition((x+50) + positionObject.visual.draw.width + 70, y+70);
	positionObject.visual.selectionTextureY.setPosition((x+50) + positionObject.visual.draw.width + 500, y);positionObject.visual.selectionDrawY.setPosition((x+50) + positionObject.visual.draw.width + 520, y+70);

	positionObject.visual.selectionDrawX.text = posObj.x+"";
	positionObject.visual.selectionDrawY.text = posObj.y+"";

	foreach(v,k in positionObject.visual){
		k.visible = true;
	}

	positionObject.visual.draw.top();
	positionObject.visual.selectionDrawX.top();
	positionObject.visual.selectionDrawY.top();

	y = y + 300;
	/////////////////////////////////////////////////////
	if(type == "TEXTURE")
	{
		local posObj = GUICreator.Elements[GUICreator.activeElement].getSize();

		sizeObject.visual.draw.setPosition(x+50,y+70);
		sizeObject.visual.selectionTextureX.setPosition((x+50) + sizeObject.visual.draw.width + 50, y);sizeObject.visual.selectionDrawX.setPosition((x+50) + sizeObject.visual.draw.width + 70, y+70);
		sizeObject.visual.selectionTextureY.setPosition((x+50) + sizeObject.visual.draw.width + 500, y);sizeObject.visual.selectionDrawY.setPosition((x+50) + sizeObject.visual.draw.width + 520, y+70);

		sizeObject.visual.selectionDrawX.text = posObj.width+"";
		sizeObject.visual.selectionDrawY.text = posObj.height+"";

		foreach(v,k in sizeObject.visual){
			k.visible = true;
		}

		sizeObject.visual.draw.top();
		sizeObject.visual.selectionDrawX.top();
		sizeObject.visual.selectionDrawY.top();

		y = y + 300;
	}else{
		foreach(v,k in sizeObject.visual){
			k.visible = false;
		}
	}
	/////////////////////////////////////////////////////
	if(type == "DRAW")
	{
		local text = GUICreator.Elements[GUICreator.activeElement].getText();

		textObject.visual.draw.setPosition(x+50,y+70);
		textObject.visual.selectionTexture.setPosition((x+50) + textObject.visual.draw.width + 50, y);textObject.visual.selectionDraw.setPosition((x+50) + textObject.visual.draw.width + 70, y+70);
		textObject.visual.selectionDraw.text = text+"";

		foreach(v,k in textObject.visual){
			k.visible = true;
		}

		textObject.visual.draw.top();
		textObject.visual.selectionDraw.top();

		y = y + 300;
	}else{
		foreach(v,k in textObject.visual){
			k.visible = false;
		}
	}
	/////////////////////////////////////////////////////
	local colorObj = GUICreator.Elements[GUICreator.activeElement].getColor();

	colorObject.visual.draw.setPosition(x+50,y+70);
	colorObject.visual.selectionTextureR.setPosition((x+50) + colorObject.visual.draw.width + 50, y);colorObject.visual.selectionDrawR.setPosition((x+50) + colorObject.visual.draw.width + 70, y+70);
	colorObject.visual.selectionTextureG.setPosition((x+50) + colorObject.visual.draw.width + 500, y);colorObject.visual.selectionDrawG.setPosition((x+50) + colorObject.visual.draw.width + 520, y+70);
	colorObject.visual.selectionTextureB.setPosition((x+50) + colorObject.visual.draw.width + 950, y);colorObject.visual.selectionDrawB.setPosition((x+50) + colorObject.visual.draw.width + 970, y+70);

	colorObject.visual.selectionDrawR.text = colorObj.r+"";
	colorObject.visual.selectionDrawG.text = colorObj.g+"";
	colorObject.visual.selectionDrawB.text = colorObj.b+"";

	foreach(v,k in colorObject.visual){
		k.visible = true;
	}

	colorObject.visual.draw.top();
	colorObject.visual.selectionDrawR.top();
	colorObject.visual.selectionDrawG.top();
	colorObject.visual.selectionDrawB.top();

	y = y + 300;
	/////////////////////////////////////////////////////
	local alphaObj = GUICreator.Elements[GUICreator.activeElement].getAlpha();

	alphaObject.visual.draw.setPosition(x+50,y+70);
	alphaObject.visual.selectionTexture.setPosition((x+50) + alphaObject.visual.draw.width + 50, y);alphaObject.visual.selectionDraw.setPosition((x+50) + alphaObject.visual.draw.width + 70, y+70);
	alphaObject.visual.selectionDraw.text = alphaObj+"";

	foreach(v,k in alphaObject.visual){
		k.visible = true;
	}

	alphaObject.visual.draw.top();
	alphaObject.visual.selectionDraw.top();

	y = y + 300;
	/////////////////////////////////////////////////////
	if(type == "TEXTURE")
	{
		local rectObj = GUICreator.Elements[GUICreator.activeElement].getRect();

		rectObject.visual.draw.setPosition(x+50,y+70);
		rectObject.visual.selectionTextureX.setPosition((x+50) + rectObject.visual.draw.width + 50, y);rectObject.visual.selectionDrawX.setPosition((x+50) + rectObject.visual.draw.width + 70, y+70);
		rectObject.visual.selectionTextureY.setPosition((x+50) + rectObject.visual.draw.width + 500, y);rectObject.visual.selectionDrawY.setPosition((x+50) + rectObject.visual.draw.width + 520, y+70);
		rectObject.visual.selectionTextureWIDTH.setPosition((x+50) + rectObject.visual.draw.width + 950, y);rectObject.visual.selectionDrawWIDTH.setPosition((x+50) + rectObject.visual.draw.width + 970, y+70);
		rectObject.visual.selectionTextureHEIGHT.setPosition((x+50) + rectObject.visual.draw.width + 1400, y);rectObject.visual.selectionDrawHEIGHT.setPosition((x+50) + rectObject.visual.draw.width + 1420, y+70);

		rectObject.visual.selectionDrawX.text = rectObj.x+"";
		rectObject.visual.selectionDrawY.text = rectObj.y+"";
		rectObject.visual.selectionDrawWIDTH.text = rectObj.width+"";
		rectObject.visual.selectionDrawHEIGHT.text = rectObj.height+"";

		foreach(v,k in rectObject.visual){
			k.visible = true;
		}

		rectObject.visual.draw.top();
		rectObject.visual.selectionDrawX.top();
		rectObject.visual.selectionDrawY.top();
		rectObject.visual.selectionDrawWIDTH.top();
		rectObject.visual.selectionDrawHEIGHT.top();

		y = y + 300;
	}else{
		foreach(v,k in rectObject.visual){
			k.visible = false;
		}
	}
	/////////////////////////////////////////////////////
	otherButtons.visual.buttonCopy.setPosition((x+50), y);otherButtons.visual.drawCopy.setPosition((x+70), y+40);
	otherButtons.visual.buttonTop.setPosition((x+1150), y);otherButtons.visual.drawTop.setPosition((x+1170), y+40);

	foreach(v,k in otherButtons.visual){
		k.visible = true;
	}

	otherButtons.visual.drawCopy.top();
	otherButtons.visual.drawTop.top();

	y = y + 300;
	/////////////////////////////////////////////////////
	selectTextureOperator.clear();
	activeButtonOperator = false;
	textureOperate.setPosition(x+50, y);textureOperate.visible = true;
	drawOperate.setPosition(x + textureOperate.getSize().width/2 + 50 - drawOperate.width/2, y+50);drawOperate.visible = true; drawOperate.top();
	drawOperate.text = type == "TEXTURE" ? "Tekstura" : "Czcionka";
	/////////////////////////////////////////////////////
}

function setActiveType(type)
{
	activeType = type;
}

function hideCurrentGuiState()
{
	foreach(v,k in sizeObject.visual)
		k.visible = false;
	
	foreach(v,k in positionObject.visual)
		k.visible = false;
		
	foreach(v,k in alphaObject.visual)
		k.visible = false;
	
	foreach(v,k in colorObject.visual)
		k.visible = false;
	
	foreach(v,k in rectObject.visual)
		k.visible = false;

	foreach(v,k in textObject.visual)
		k.visible = false;
	
	foreach(v,k in otherButtons.visual)
		k.visible = false;

	
	textureOperate.visible = false;
	drawOperate.visible = false;
	activeButtonOperator = false;
	selectedButtonOperator = false;
	selectTextureOperator.clear();
}

function reloadGUIState()
{
showCurrentGuiState(lastLoadedPosition.x, lastLoadedPosition.y, activeType);
}

local function onRenderHandler()
{
	/////////////////////////////////////////////////////
	if(textureOperate.visible)
	{
		if(ControllerInit.isMouseOnTexture(textureOperate))
		{
			if(activeButtonOperator == false){
				activeButtonOperator = true;
				textureOperate.setAlpha(200);
			}
		}else{
			if(activeButtonOperator){
				activeButtonOperator = false;
				textureOperate.setAlpha(150);
				if(selectedButtonOperator){
					selectedButtonOperator = false;
					textureOperate.setSize(2000,200);
					drawOperate.top();
					selectTextureOperator.clear();
				}
			}
		}
	}
	/////////////////////////////////////////////////////
}


addEventHandler("ControllerInit.onFps",onRenderHandler);

local function keyHandler(key)
{
	if(GUICreator.activeElement != null)
	{
		if(key == KEY_Z)
		{
			GUICreator.Line.len() == 0 ? GUICreator.DrawLineOutsideElement(GUICreator.Elements[GUICreator.activeElement]) : GUICreator.HideLineOutsides();
		}
	}

	if(textObject.active)
	{
		if(key == KEY_BACK)
		{
			if(textObject.visual.selectionDraw.text.len() > 0)
			{
				textObject.visual.selectionDraw.text = textObject.visual.selectionDraw.text.slice(0, textObject.visual.selectionDraw.text.len() - 1);
			}
		}
		if(getKeyLetter(key) != null)
		{
			if(addKey(textObject.visual.selectionDraw.text, getKeyLetter(key), "TEXT", 50, 13))
			{
				GUICreator.Elements[GUICreator.activeElement].setText(textObject.visual.selectionDraw.text);
			}
		}
	}
	////////////////////////////////////////////////////////////////////////
	if(rectObject.activeX)
	{
		if(key == KEY_BACK)
		{
			if(rectObject.visual.selectionDrawX.text.len() > 0)
			{
				rectObject.visual.selectionDrawX.text = rectObject.visual.selectionDrawX.text.slice(0, rectObject.visual.selectionDrawX.text.len() - 1);
			}
		}
		if(getKeyLetter(key) != null)
		{
			if(addKey(rectObject.visual.selectionDrawX.text, getKeyLetter(key), "NUMBER", 5, 9))
			{
				local posObj = GUICreator.Elements[GUICreator.activeElement].getRect();
				GUICreator.Elements[GUICreator.activeElement].setRect(rectObject.visual.selectionDrawX.text.tointeger(), posObj.y, posObj.width, posObj.height);
			}
		}
	}
	else if(rectObject.activeY)
	{
		if(key == KEY_BACK)
		{
			if(rectObject.visual.selectionDrawY.text.len() > 0)
			{
				rectObject.visual.selectionDrawY.text = rectObject.visual.selectionDrawY.text.slice(0, rectObject.visual.selectionDrawY.text.len() - 1);
			}
		}
		if(getKeyLetter(key) != null)
		{
			if(addKey(rectObject.visual.selectionDrawY.text, getKeyLetter(key), "NUMBER", 5, 10))
			{
				local posObj = GUICreator.Elements[GUICreator.activeElement].getRect();
				GUICreator.Elements[GUICreator.activeElement].setRect(posObj.x, rectObject.visual.selectionDrawY.text.tointeger(),posObj.width, posObj.height);
			}
		}
	}
	else if(rectObject.activeWIDTH)
	{
		if(key == KEY_BACK)
		{
			if(rectObject.visual.selectionDrawWIDTH.text.len() > 0)
			{
				rectObject.visual.selectionDrawWIDTH.text = rectObject.visual.selectionDrawWIDTH.text.slice(0, rectObject.visual.selectionDrawWIDTH.text.len() - 1);
			}
		}
		if(getKeyLetter(key) != null)
		{
			if(addKey(rectObject.visual.selectionDrawWIDTH.text, getKeyLetter(key), "NUMBER", 5, 11))
			{
				local posObj = GUICreator.Elements[GUICreator.activeElement].getRect();
				GUICreator.Elements[GUICreator.activeElement].setRect(posObj.x, posObj.y,rectObject.visual.selectionDrawWIDTH.text.tointeger(), posObj.height);
			}
		}
	}
	else if(rectObject.activeHEIGHT)
	{
		if(key == KEY_BACK)
		{
			if(rectObject.visual.selectionDrawHEIGHT.text.len() > 0)
			{
				rectObject.visual.selectionDrawHEIGHT.text = rectObject.visual.selectionDrawHEIGHT.text.slice(0, rectObject.visual.selectionDrawHEIGHT.text.len() - 1);
			}
		}
		if(getKeyLetter(key) != null)
		{
			if(addKey(rectObject.visual.selectionDrawHEIGHT.text, getKeyLetter(key), "NUMBER", 5, 12))
			{
				local posObj = GUICreator.Elements[GUICreator.activeElement].getRect();
				GUICreator.Elements[GUICreator.activeElement].setRect(posObj.x,posObj.height,posObj.width, rectObject.visual.selectionDrawHEIGHT.text.tointeger());
			}
		}
	}
	////////////////////////////////////////////////////////////////////////
	if(alphaObject.active)
	{
		if(key == KEY_BACK)
		{
			if(alphaObject.visual.selectionDraw.text.len() > 0)
			{
				alphaObject.visual.selectionDraw.text = alphaObject.visual.selectionDraw.text.slice(0, alphaObject.visual.selectionDraw.text.len() - 1);
			}
		}
		if(getKeyLetter(key) != null)
		{
			if(addKey(alphaObject.visual.selectionDraw.text, getKeyLetter(key), "NUMBER", 3, 8))
			{
				GUICreator.Elements[GUICreator.activeElement].setAlpha(alphaObject.visual.selectionDraw.text.tointeger());
			}
		}
	}
	////////////////////////////////////////////////////////////////////////
	if(positionObject.activeX)
	{
		if(key == KEY_BACK)
		{
			if(positionObject.visual.selectionDrawX.text.len() > 0)
			{
				positionObject.visual.selectionDrawX.text = positionObject.visual.selectionDrawX.text.slice(0, positionObject.visual.selectionDrawX.text.len() - 1);
			}
		}
		if(getKeyLetter(key) != null)
		{
			if(addKey(positionObject.visual.selectionDrawX.text, getKeyLetter(key), "NUMBER", 4, 4))
			{
				local posObj = GUICreator.Elements[GUICreator.activeElement].getPosition();
				GUICreator.Elements[GUICreator.activeElement].setPosition(positionObject.visual.selectionDrawX.text.tointeger(), posObj.y);
			}
		}
	}
	else if(positionObject.activeY)
	{
		if(key == KEY_BACK)
		{
			if(positionObject.visual.selectionDrawY.text.len() > 0)
			{
				positionObject.visual.selectionDrawY.text = positionObject.visual.selectionDrawY.text.slice(0, positionObject.visual.selectionDrawY.text.len() - 1);
			}
		}
		if(getKeyLetter(key) != null)
		{
			if(addKey(positionObject.visual.selectionDrawY.text, getKeyLetter(key), "NUMBER", 4, 5))
			{
				local posObj = GUICreator.Elements[GUICreator.activeElement].getPosition();
				GUICreator.Elements[GUICreator.activeElement].setPosition(posObj.x, positionObject.visual.selectionDrawY.text.tointeger());
			}
		}
	}
	////////////////////////////////////////////////////////////////////////
	if(sizeObject.activeX)
	{
		if(key == KEY_BACK)
		{
			if(sizeObject.visual.selectionDrawX.text.len() > 0)
			{
				sizeObject.visual.selectionDrawX.text = sizeObject.visual.selectionDrawX.text.slice(0, sizeObject.visual.selectionDrawX.text.len() - 1);
			}
		}
		if(getKeyLetter(key) != null)
		{
			if(addKey(sizeObject.visual.selectionDrawX.text, getKeyLetter(key), "NUMBER", 4, 6))
			{
				local posObj = GUICreator.Elements[GUICreator.activeElement].getSize();
				GUICreator.Elements[GUICreator.activeElement].setSize(sizeObject.visual.selectionDrawX.text.tointeger(), posObj.height);
			}
		}
	}
	else if(sizeObject.activeY)
	{
		if(key == KEY_BACK)
		{
			if(sizeObject.visual.selectionDrawY.text.len() > 0)
			{
				sizeObject.visual.selectionDrawY.text = sizeObject.visual.selectionDrawY.text.slice(0, sizeObject.visual.selectionDrawY.text.len() - 1);
			}
		}
		if(getKeyLetter(key) != null)
		{
			if(addKey(sizeObject.visual.selectionDrawY.text, getKeyLetter(key), "NUMBER", 4, 7))
			{
				local posObj = GUICreator.Elements[GUICreator.activeElement].getSize();
				GUICreator.Elements[GUICreator.activeElement].setSize(posObj.width, sizeObject.visual.selectionDrawY.text.tointeger());
			}
		}
	}
	////////////////////////////////////////////////////////////////////////
	if(colorObject.activeR)
	{
		if(key == KEY_BACK)
		{
			if(colorObject.visual.selectionDrawR.text.len() > 0)
			{
				colorObject.visual.selectionDrawR.text = colorObject.visual.selectionDrawR.text.slice(0, colorObject.visual.selectionDrawR.text.len() - 1);
			}
		}
		if(getKeyLetter(key) != null)
		{
			if(addKey(colorObject.visual.selectionDrawR.text, getKeyLetter(key), "NUMBER", 3, 1))
			{
				local posObj = GUICreator.Elements[GUICreator.activeElement].getColor();
				GUICreator.Elements[GUICreator.activeElement].setColor(colorObject.visual.selectionDrawR.text.tointeger(), posObj.g, posObj.b);
			}
		}
	}
	else if(colorObject.activeG)
	{
		if(key == KEY_BACK)
		{
			if(colorObject.visual.selectionDrawG.text.len() > 0)
			{
				colorObject.visual.selectionDrawG.text = colorObject.visual.selectionDrawG.text.slice(0, colorObject.visual.selectionDrawG.text.len() - 1);
			}
		}
		if(getKeyLetter(key) != null)
		{
			if(addKey(colorObject.visual.selectionDrawG.text, getKeyLetter(key), "NUMBER", 3, 2))
			{
				local posObj = GUICreator.Elements[GUICreator.activeElement].getColor();
				GUICreator.Elements[GUICreator.activeElement].setColor(posObj.r, colorObject.visual.selectionDrawG.text.tointeger(), posObj.b);
			}
		}
	}
	else if(colorObject.activeB)
	{
		if(key == KEY_BACK)
		{
			if(colorObject.visual.selectionDrawB.text.len() > 0)
			{
				colorObject.visual.selectionDrawB.text = colorObject.visual.selectionDrawB.text.slice(0, colorObject.visual.selectionDrawB.text.len() - 1);
			}
		}
		if(getKeyLetter(key) != null)
		{
			if(addKey(colorObject.visual.selectionDrawB.text, getKeyLetter(key), "NUMBER", 3, 3))
			{
				local posObj = GUICreator.Elements[GUICreator.activeElement].getColor();
				GUICreator.Elements[GUICreator.activeElement].setColor(posObj.r, posObj.g, colorObject.visual.selectionDrawB.text.tointeger());
			}
		}
	}
	////////////////////////////////////////////////////////////////////////
}

addEventHandler("onKey", keyHandler);

local function clickMouseHandler(btn)
{
	if(ControllerInit.isMouseOnTexture(otherButtons.visual.buttonCopy)){
		GUICreator.CopyCurrentElement(activeType);
	}
	if(ControllerInit.isMouseOnTexture(otherButtons.visual.buttonTop)){
		GUICreator.SlideUpCurrentElement();
	}	
	/////////////////////////////////////////////////////////////////////////////
	if(textObject.active)
	{
		if(!ControllerInit.isMouseOnTexture(textObject.visual.selectionTexture)){
			textObject.active = false;
			textObject.visual.selectionTexture.setColor(0,0,0);
		}
	}else{
		if(ControllerInit.isMouseOnTexture(textObject.visual.selectionTexture))
		{
			textObject.active = true;
			textObject.visual.selectionTexture.setColor(200,0,0);
		}
	}
	///////////////////////////////////////////////////////////////////
	if(rectObject.activeX)
	{
		if(!ControllerInit.isMouseOnTexture(rectObject.visual.selectionTextureX)){
			rectObject.activeX = false;
			rectObject.visual.selectionTextureX.setColor(0,0,0);
		}
	}else{
		if(ControllerInit.isMouseOnTexture(rectObject.visual.selectionTextureX))
		{
			rectObject.activeX = true;
			rectObject.visual.selectionTextureX.setColor(200,0,0);
		}
	}
	if(rectObject.activeY)
	{
		if(!ControllerInit.isMouseOnTexture(rectObject.visual.selectionTextureY)){
			rectObject.activeY = false;
			rectObject.visual.selectionTextureY.setColor(0,0,0);
		}
	}else{
		if(ControllerInit.isMouseOnTexture(rectObject.visual.selectionTextureY))
		{
			rectObject.activeY = true;
			rectObject.visual.selectionTextureY.setColor(200,0,0);
		}
	}
	if(rectObject.activeWIDTH)
	{
		if(!ControllerInit.isMouseOnTexture(rectObject.visual.selectionTextureWIDTH)){
			rectObject.activeWIDTH = false;
			rectObject.visual.selectionTextureWIDTH.setColor(0,0,0);
		}
	}else{
		if(ControllerInit.isMouseOnTexture(rectObject.visual.selectionTextureWIDTH))
		{
			rectObject.activeWIDTH = true;
			rectObject.visual.selectionTextureWIDTH.setColor(200,0,0);
		}
	}
	if(rectObject.activeHEIGHT)
	{
		if(!ControllerInit.isMouseOnTexture(rectObject.visual.selectionTextureHEIGHT)){
			rectObject.activeHEIGHT = false;
			rectObject.visual.selectionTextureHEIGHT.setColor(0,0,0);
		}
	}else{
		if(ControllerInit.isMouseOnTexture(rectObject.visual.selectionTextureHEIGHT))
		{
			rectObject.activeHEIGHT = true;
			rectObject.visual.selectionTextureHEIGHT.setColor(200,0,0);
		}
	}
	///////////////////////////////////////////////////////////////////
	if(alphaObject.active)
	{
		if(!ControllerInit.isMouseOnTexture(alphaObject.visual.selectionTexture)){
			alphaObject.active = false;
			alphaObject.visual.selectionTexture.setColor(0,0,0);
		}
	}else{
		if(ControllerInit.isMouseOnTexture(alphaObject.visual.selectionTexture))
		{
			alphaObject.active = true;
			alphaObject.visual.selectionTexture.setColor(200,0,0);
		}
	}
	///////////////////////////////////////////////////////////////////
	if(positionObject.activeX)
	{
		if(!ControllerInit.isMouseOnTexture(positionObject.visual.selectionTextureX)){
			positionObject.activeX = false;
			positionObject.visual.selectionTextureX.setColor(0,0,0);
		}
	}else{
		if(ControllerInit.isMouseOnTexture(positionObject.visual.selectionTextureX))
		{
			positionObject.activeX = true;
			positionObject.visual.selectionTextureX.setColor(200,0,0);
		}
	}
	if(positionObject.activeY)
	{
		if(!ControllerInit.isMouseOnTexture(positionObject.visual.selectionTextureY)){
			positionObject.activeY = false;
			positionObject.visual.selectionTextureY.setColor(0,0,0);
		}
	}else{
		if(ControllerInit.isMouseOnTexture(positionObject.visual.selectionTextureY))
		{
			positionObject.activeY = true;
			positionObject.visual.selectionTextureY.setColor(200,0,0);
		}
	}
	///////////////////////////////////////////////////////////////////
	if(sizeObject.activeX)
	{
		if(!ControllerInit.isMouseOnTexture(sizeObject.visual.selectionTextureX)){
			sizeObject.activeX = false;
			sizeObject.visual.selectionTextureX.setColor(0,0,0);
		}
	}else{
		if(ControllerInit.isMouseOnTexture(sizeObject.visual.selectionTextureX))
		{
			sizeObject.activeX = true;
			sizeObject.visual.selectionTextureX.setColor(200,0,0);
		}
	}
	if(sizeObject.activeY)
	{
		if(!ControllerInit.isMouseOnTexture(sizeObject.visual.selectionTextureY)){
			sizeObject.activeY = false;
			sizeObject.visual.selectionTextureY.setColor(0,0,0);
		}
	}else{
		if(ControllerInit.isMouseOnTexture(sizeObject.visual.selectionTextureY))
		{
			sizeObject.activeY = true;
			sizeObject.visual.selectionTextureY.setColor(200,0,0);
		}
	}
	///////////////////////////////////////////////////////////////////
	if(colorObject.activeR)
	{
		if(!ControllerInit.isMouseOnTexture(colorObject.visual.selectionTextureR)){
			colorObject.activeR = false;
			colorObject.visual.selectionTextureR.setColor(0,0,0);
		}
	}else{
		if(ControllerInit.isMouseOnTexture(colorObject.visual.selectionTextureR))
		{
			colorObject.activeR = true;
			colorObject.visual.selectionTextureR.setColor(200,0,0);
		}
	}
	if(colorObject.activeG)
	{
		if(!ControllerInit.isMouseOnTexture(colorObject.visual.selectionTextureG)){
			colorObject.activeG = false;
			colorObject.visual.selectionTextureG.setColor(0,0,0);
		}
	}else{
		if(ControllerInit.isMouseOnTexture(colorObject.visual.selectionTextureG))
		{
			colorObject.activeG = true;
			colorObject.visual.selectionTextureG.setColor(200,0,0);
		}
	}
	if(colorObject.activeB)
	{
		if(!ControllerInit.isMouseOnTexture(colorObject.visual.selectionTextureB)){
			colorObject.activeB = false;
			colorObject.visual.selectionTextureB.setColor(0,0,0);
		}
	}else{
		if(ControllerInit.isMouseOnTexture(colorObject.visual.selectionTextureB))
		{
			colorObject.activeB = true;
			colorObject.visual.selectionTextureB.setColor(200,0,0);
		}
	}
	/////////////////////////////////////////////////////
	if(activeButtonOperator)
	{
		if(selectedButtonOperator)
		{
			foreach(id,val in selectTextureOperator)
			{
				if(ControllerInit.isMouseOnTexture(val.texture)){
					switch(activeType)
					{
						case "TEXTURE":
							GUICreator.Elements[GUICreator.activeElement].setTexture(GUICreator.texturesSelect[id]);
						break;
						case "DRAW":
							GUICreator.Elements[GUICreator.activeElement].setFont(GUICreator.drawSelect[id]);
						break;
					}
				}
			}
		}else{
			selectedButtonOperator = true;
			textureOperate.top();
			drawOperate.top();

			local height = textureOperate.getSize().height;
			local posEl = textureOperate.getPosition();

			local localTable = null;

			switch(activeType)
			{
				case "TEXTURE":
					localTable = GUICreator.texturesSelect;
				break;
				case "DRAW":
					localTable = GUICreator.drawSelect;
				break;
			}

			foreach(value in localTable){
				selectTextureOperator.append({texture = Texture(posEl.x,posEl.y + height, 2000, 200, "SELECT.TGA"), draw = Draw(posEl.x+50, posEl.y + height + 70, value)});
				selectTextureOperator[selectTextureOperator.len() - 1].texture.visible = true;selectTextureOperator[selectTextureOperator.len() - 1].texture.setColor(0,0,0);
				selectTextureOperator[selectTextureOperator.len() - 1].draw.visible = true;selectTextureOperator[selectTextureOperator.len() - 1].draw.top();
				height = height + 220;
			}
			textureOperate.setSize(2000,height);
		}
	}
	/////////////////////////////////////////////////////
}
addEventHandler("onMouseClick", clickMouseHandler);

function getHeightGuiState()
{
	if(activeType == "TEXTURE")
	{
		return 2100;
	}
	return 1500;
}

function addKey(text,letter, type, max, change)
{
	if(type == "NUMBER")
	{
		if(letter == "0" || letter == "1" || letter == "2" || letter == "3" || letter == "4" || letter == "5" || letter == "6" || letter == "7" || letter == "8" || letter == "9" || letter == "0")
		{}else{
			return false;
		}
	}
	local lenText = text.len();
	if(lenText >= max)
	{
		return false;
	}

	switch(change)
	{
		case 1: colorObject.visual.selectionDrawR.text = text + ""+letter; break;
		case 2: colorObject.visual.selectionDrawG.text = text + ""+letter; break;
		case 3: colorObject.visual.selectionDrawB.text = text + ""+letter; break;
		case 4: positionObject.visual.selectionDrawX.text = text + ""+letter; break;
		case 5: positionObject.visual.selectionDrawY.text = text + ""+letter; break;
		case 6: sizeObject.visual.selectionDrawX.text = text + ""+letter; break;
		case 7: sizeObject.visual.selectionDrawY.text = text + ""+letter; break;
		case 8: alphaObject.visual.selectionDraw.text = text + ""+letter; break;
		case 9: rectObject.visual.selectionDrawX.text = text + ""+letter; break;
		case 10: rectObject.visual.selectionDrawY.text = text + ""+letter; break;
		case 11: rectObject.visual.selectionDrawWIDTH.text = text + ""+letter; break;
		case 12: rectObject.visual.selectionDrawHEIGHT.text = text + ""+letter; break;
		case 13: textObject.visual.selectionDraw.text = text + ""+letter; break;
	}

	return true;
}

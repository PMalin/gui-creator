
/**
Author: Quarchodron (http://gothic-online.com.pl/forum/member.php?action=profile&uid=15)
CC ( 2019 )
**/

const TYPE_MOVE_LEFT = 0;
const TYPE_MOVE_RIGHT = 1;
const TYPE_MOVE_TOP = 2;
const TYPE_MOVE_BOTTOM = 3;
const TYPE_MOVE_CHANGE_POSITION = 4;

class ElementTexture extends Element
{
	operandMove = null;	
	optionalOptions = null;
	changedRect = false;
	infoPing = false;
	pingStats = null;
	
	constructor()
	{
		base.constructor();
		operandMove = { active =false, posx = 0, posy = 0, type=0 };	
		optionalOptions = {rectX = 0, rectY = 0};		
		element = Texture(3500,3500, 500, 500,"SELECT.TGA");
		changedRect = false;
		infoPing = false;
		pingStats = {x = 0, y = 0};
	}
	
	function setPosition(x,y)
	{
		if(x == "" || x == null) x = 0;
		if(y == "" || y == null) y = 0;
		element.setPosition(x,y);
		local tab = getRect();
		if(changedRect){
		element.setRect(tab.x, tab.y, tab.width, tab.height);
		}
	}	
	
	function ping(initialize = true)
	{
		if(initialize){
			local pos = element.getPosition();
			infoPing = 50;
			pingStats = {x = pos.x, y = pos.y};
			element.setPosition(x,y);
		}
		
		if(infoPing != false)
		{
			element.setPosition(pingStats.x - 50 + rand() % 100,pingStats.y - 50 + rand() % 100);
			infoPing = infoPing - 1;
			if(infoPing < 0){
				element.setPosition(pingStats.x, pingStats.y);
				infoPing = false;
			}
		}
	}
	
	function getTexture()
	{
		return element.file;
	}
	
	function setTexture(val)
	{
		element.file = val;
	}
	
	function getRect()
	{
		local rectTab = element.getRect();
		
		return {width = rectTab.width, height = rectTab.height, x = optionalOptions.rectX, y = optionalOptions.rectY};
	}
	
	function setRect(x,y,width,height)
	{
		if(x == "" || x == null) x = 0;
		if(y == "" || y == null) y = 0;
		if(width == "" || width == null) width = 0;
		if(height == "" || height == null) height = 0;	
		
		changedRect = true;
		
		optionalOptions.rectX = x;
		optionalOptions.rectY = y;
				
		element.setRect(x,y,width,height);
	}
	
	function getSize()
	{
		return element.getSize();
	}
	
	function setSize(width,height)
	{
		if(width == "" || width == null) width = 0;
		if(height == "" || height == null) height = 0;
		element.setSize(width,height);
		local tab = getRect();
		if(changedRect){
		element.setRect(tab.x, tab.y, tab.width, tab.height);
		}
	}
	
	function getInfo()
	{
		return "Texture";
	}

	function changWidthCursor(ch=false)
	{
		local cur = getCursorPosition();
		local sizeElement = element.getSize();
		local positionElement = element.getPosition();
		local curPos = cur.x + operandMove.posx;
		if(ch){
			element.setPosition(curPos, positionElement.y);
			element.setSize(sizeElement.width + (positionElement.x - curPos), sizeElement.height);
		}else{
			element.setSize(cur.x - positionElement.x, sizeElement.height);
		}
	}
	
	function changHeightCursor(ch=false)
	{
		local cur = getCursorPosition();
		local sizeElement = element.getSize();
		local positionElement = element.getPosition();
		local curPos = cur.y + operandMove.posy;	
		if(ch){
			element.setPosition(positionElement.x, curPos);
			element.setSize(sizeElement.width,sizeElement.height + (positionElement.y - curPos));	
		}else{
			element.setSize(sizeElement.width,cur.y - positionElement.y);	
		}
	}
	
	function fpsEvent()
	{
		if(element.visible)
		{
			if(operandMove.active)
			{
				if(isMouseBtnPressed(MOUSE_LMB))
				{
					switch(operandMove.type)
					{
						case TYPE_MOVE_LEFT:
							this.changWidthCursor(true);
						break;
						case TYPE_MOVE_RIGHT:
							this.changWidthCursor();
						break;		
						case TYPE_MOVE_BOTTOM:
							this.changHeightCursor();
						break;
						case TYPE_MOVE_TOP:
							this.changHeightCursor(true);
						break;						
						default:
							this.followCursor();
						break;
					}
					reloadGUIState();
				}else{
					operandMove.active = false;
				}
			}
		}
		ping(false);
	}
	
	function clickEvent(btn)
	{
		if(element.visible)
		{
			if(btn == MOUSE_LMB)
			{
				if(ControllerInit.isMouseOnTexture(element))
				{
					local cur = getCursorPosition();
					local pos = element.getPosition();
					local size = element.getSize();
					operandMove.posx = pos.x - cur.x;
					operandMove.posy = pos.y - cur.y;
					operandMove.active = true;
					if(cur.x >= pos.x-20 && cur.x <= pos.x+20)
					{
						operandMove.type = TYPE_MOVE_LEFT;
					}else if(cur.x >= (pos.x+size.width)-20 && cur.x <= (pos.x+size.width)+20)
					{
						operandMove.type = TYPE_MOVE_RIGHT;
					}else if(cur.y >= pos.y-20 && cur.y <= pos.y+20)
					{
						operandMove.type = TYPE_MOVE_TOP;
					}else if(cur.y >= (pos.y+size.height)-20 && cur.y <= (pos.y+size.height)+20)
					{
						operandMove.type = TYPE_MOVE_BOTTOM;
					}else{
						operandMove.type = TYPE_MOVE_CHANGE_POSITION;
					}
				}
			}
		}		
	}	
	
	function extendedGuiPrepare(x,y)
	{	
		setActiveType("TEXTURE");
		showCurrentGuiState(x,y,"TEXTURE");
	}
	
	function getLinesToSave()
	{
		local retArra = [];
		local pos = element.getPosition();
		local size = element.getSize();
		local rect = getRect();
		local posExtend = element.getPositionPx();
		local sizeExtend = element.getSizePx();
		local color = element.getColor();
		retArra.append("local text = Texture("+pos.x+","+pos.y+","+size.width+","+size.height+",\""+element.file+"\");");
		if(changedRect == true){
			retArra.append("text.setRect("+rect.x+","+rect.y+","+rect.width+","+rect.height+");");
		}
		if(changedColor == true){
			retArra.append("text.setColor("+color.r+","+color.g+","+color.b+");");
		}	
		if(changedAlpha == true){
			retArra.append("text.setAlpha("+element.getAlpha()+");");
		}
		retArra.append("//text.setPositionPx("+posExtend.x+","+posExtend.y+");");
		retArra.append("//text.setSizePx("+sizeExtend.width+","+sizeExtend.height+");");
		if(changedRect == true){
			retArra.append("//text.setRectPx("+anx(rect.x)+","+any(rect.y)+","+anx(rect.width)+","+any(rect.height)+");");
		}		
		return retArra;
	}
	
	function getLinesToTemplate()
	{
		local pos = element.getPosition();
		local size = getSize();
		local color = element.getColor();
		local rect = getRect();
		return size.width + " " + size.height + " " + pos.x+" "+pos.y +" "+color.r+" "+color.g+" "+color.b+ " " + element.file + " " + boolToInt(changedAlpha) + " " + element.getAlpha()+ " " + boolToInt(changedRect) + " " + rect.x + " " + rect.y + " " + rect.width + " " + rect.height;
	}
	
	static function checkISItTextureTemplate(template)
	{
		local arg = sscanf("dddddddsddddddd", template);
		if(arg) { 
			return true;
		}
		return false;
	}
	
	function loadTemplate(template)
	{
		local arg = sscanf("dddddddsddddddd", template);
		if(arg) { 
			element.setSize(arg[0], arg[1]);
			element.setPosition(arg[2], arg[3]);
			element.setColor(arg[4], arg[5], arg[6]);
			element.file = arg[7];
			if(arg[8] == 1){
				changedAlpha = true;
				element.setAlpha(arg[9]);
			}
			if(arg[10] == 1){
				changedRect = true;
				setRect(arg[11],arg[12],arg[13],arg[14]);
			}			
		}			
	}	
}
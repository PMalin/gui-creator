
/**
Author: Quarchodron (http://gothic-online.com.pl/forum/member.php?action=profile&uid=15)
CC ( 2019 )
**/

local popDownedElements = 0;

class ControllerInit.Menu 
{
	element = null;
	active = null;
	operands = null;
	elements = null;
	visible = false;
	extendElement = null;
	operandMove = null;
	
	constructor(name,x,y,_r=255,_g=255,_b=255)
	{
		elements = [];
		visible = false;
		active = true;
		extendElement = null;
		operandMove = { active =false, posx = 0, posy = 0 };
		
		operands = [
			Texture(0,0,0,0,"SELECT.TGA"),
			Texture(0,0,0,0,"SELECT.TGA"),
			Draw(0,0, "_"),
			Draw(0,0, name),
		];
		
		element = Texture(x,y,2000,200,"SELECT.TGA");
		element.setColor(_r, _g, _b);
		element.setAlpha(190);
		
        local _this = this;
        addEventHandler("ControllerInit.onFps", function(){ _this.fpsEvent() });		
        addEventHandler("ControllerInit.onClick", function(btn){ _this.clickEvent(btn) });				
	}
	
	// show controller
	function show()
	{
		visible = true;
		element.visible = visible;
		resizeMenu();
		renderElementsFromTable();
		scaleOperand();
	}
	
	// hide controller
	function hide()
	{
		visible = false;
		
		element.visible = visible;	
		foreach(val in elements) { val.draw.visible = visible; val.texture.visible = visible;};
		foreach(val in operands) { val.visible = visible; };
		
		if(extendElement != null){
			extendElement.hidePreparedGui();
		}
	}
		
	function renderElementsFromTable()
	{
		local posEl = element.getPosition()
		local sizeEl = element.getSize();
		local realSizeY = 250;
		
		foreach(val in elements)
		{
			local sizeVa = val.texture.getSize();
			
			val.texture.setPosition(posEl.x + sizeEl.width/2 - sizeVa.width/2, posEl.y + realSizeY);
			realSizeY = realSizeY + sizeVa.height + 50;
			
			val.texture.visible = true;val.texture.setColor(0,0,0);val.texture.setAlpha(150);
			val.draw.setPosition(val.texture.getPosition().x + val.texture.getSize().width/2 - val.draw.width/2, val.texture.getPosition().y + val.texture.getSize().height/2 - val.draw.height/2);val.draw.visible = true; val.draw.top();
		}
		
		if(extendElement != null)
		{
			extendElement.extendedGuiPrepare(posEl.x, posEl.y + realSizeY);
			realSizeY = realSizeY + extendElement.getHeightExtendedGui();
		}
		
		element.setSize(sizeEl.width, realSizeY);
	}
	
	function resizeMenu()
	{
		local height = element.getSize().height;
		element.setSize(getWidhestElement() + 100, 200);
	}
	
	function getWidhestElement()
	{
		local valReturn = 2000;
		foreach(val in elements)
		{
			if(val.texture.getSize().width > valReturn)
			{
				valReturn = val.texture.getSize().width;
			}
		}
		return valReturn;
	}

	function addButton(_id,width,height,text)
	{
		elements.append({id = _id, typ="BUTTON", texture = Texture(0,0,width,height,"SELECT.TGA"), draw=Draw(0,0,text), hover=false});
		if(visible)
		{
			resizeMenu();
			renderElementsFromTable();			
		}
	}
	
	function resetButtons()
	{
		elements.clear();
	}
	
	function setPosition()
	{
		local cur = getCursorPosition();
		operands[0].setPosition(cur.x + operandMove.posx, cur.y + operandMove.posy);
		local pos = operands[0].getPosition();
		element.setPosition(pos.x, pos.y);
		renderElementsFromTable();
		scaleOperand();
	}
	
	function changeExtendedElement(new) {
		if(extendElement != null)
			extendElement.destroyPreparedGui();
		
		extendElement = new;
		resizeMenu();
		renderElementsFromTable();
	}
	
	function popDown() {
		operands[2].text = "O";	
		element.visible = false;
		foreach(val in elements)
		{
			val.texture.visible = false;
			val.draw.visible = false;
		}
		if(extendElement != null){
			extendElement.hidePreparedGui();
		}
		active = false;
		
		local XOperand = popDownedElements * 1000;
		operands[0].setPosition(XOperand, 8000);
		operands[0].setSize(1000,200);
		operands[1].setPosition(XOperand + 800, 8000);
		operands[1].setSize(200,200);
		operands[2].setPosition(XOperand + 830,8010);
		operands[3].setPosition(XOperand + 30,8050);
		popDownedElements = popDownedElements + 1;
	}
	
	function popUp() {
		operands[2].text = "_";	
		element.visible = true;
		element.top();
		foreach(val in elements)
		{
			val.texture.visible = true;
			val.draw.visible = true;
			val.texture.top();
			val.draw.top();
		}
		if(extendElement != null){
			if(visible){
				resizeMenu();
				renderElementsFromTable();			
			}
		}
		active = true;
		
		scaleOperand();
		popDownedElements = popDownedElements - 1;
	}
	
	function scaleOperand() {
		local pos = element.getPosition();
		local size = element.getSize();
		operands[0].setPosition(pos.x, pos.y);operands[0].setSize(size.width, 200);operands[0].setColor(0,0,0);
		operands[1].setPosition(pos.x+size.width-200, pos.y);operands[1].setSize(200, 200);operands[1].setColor(30,30,30);	
		operands[2].font="FONT_OLD_20_WHITE_HI.TGA";operands[2].setColor(255,255,255);operands[2].setPosition(operands[1].getPosition().x + operands[1].getSize().width/2 - operands[2].width/2, operands[1].getPosition().y-100);
		operands[3].setColor(255,255,255); operands[3].setPosition(operands[0].getPosition().x+30,operands[0].getPosition().y+50);
		foreach(val in operands) { val.visible = visible; val.top()};
	}
	
	function fpsEvent()
	{
		if(visible)
		{
			if(operandMove.active)
			{
				if(isMouseBtnPressed(MOUSE_LMB))
				{
					this.setPosition();
				}else{
					operandMove.active = false;
				}
			}
			if(active)
			{
				foreach(value in elements)
				{
					if(ControllerInit.isMouseOnTexture(value.texture))
					{
						if(value.hover == false){
							value.hover = true;
							value.texture.setAlpha(200);
							continue;
						}
					}else{
						if(value.hover){
							value.hover = false;
							value.texture.setAlpha(150);
							continue;
						}
					}
				}
			}
		}
	}
	
	function clickEvent(btn)
	{
		if(visible)
		{
			if(btn == MOUSE_LMB)
			{
				if(active)
				{
					foreach(value in elements)
					{
						if(value.hover == true)
						{
							value.hover = false;
							value.texture.setAlpha(150);
							callEvent("ControllerInit.onControllerClick", this, value.id);
							break;
						}
					}
				}
				
				if(ControllerInit.isMouseOnTexture(operands[1]))
				{
					if(active)
					{
						popDown();
					}else{
						popUp();
					}
				}else if(ControllerInit.isMouseOnTexture(operands[0]))
				{
					if(active)
					{
						local cur = getCursorPosition();
						local pos = operands[0].getPosition();
						operandMove.posx = pos.x - cur.x;
						operandMove.posy = pos.y - cur.y;
						operandMove.active = true;
					}
				}
			}
		}
	}
}


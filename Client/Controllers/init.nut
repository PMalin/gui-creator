
/**
Author: Quarchodron (http://gothic-online.com.pl/forum/member.php?action=profile&uid=15)
CC ( 2019 )
**/

/**
Initialization of ControllerInit
**/

ControllerInit <- {}; //namespace

addEvent("ControllerInit.onClick");
addEvent("ControllerInit.onRelease");
addEvent("ControllerInit.onFps");
addEvent("ControllerInit.onControllerClick");

local limit = 0;
local waitFPS = 0;
local frame = 0;

/**
Functions for operation on draw and cursor.
**/

function ControllerInit::isMouseOnDraw(element)
{
	if(element == null || !element.visible){
		return;
	}
    local cur = getCursorPositionPx();
    local pos = element.getPositionPx();
    if(cur.x >= pos.x && cur.x <= pos.x + element.widthPx && cur.y >= pos.y && cur.y <= pos.y + element.heightPx)
        return true;

    return false;
}

function ControllerInit::isMouseOnTexture(element)
{
	if(element == null || !element.visible){
		return;
	}
	
    local cur = getCursorPosition();
    local pos = element.getPosition();
    local size = element.getSize();
    if(cur.x >= pos.x && cur.x <= pos.x + size.width && cur.y >= pos.y && cur.y <= pos.y + size.height)
        return true;
        
    return false;
}


/**
Callbacks to operate on controllers.
**/

local function clickMouseHandler(btn)
{
    callEvent("ControllerInit.onClick", btn);
}
addEventHandler("onMouseClick", clickMouseHandler);

local function clickReleaseHandler(btn)
{
    callEvent("ControllerInit.onRelease", btn);
}
addEventHandler("onMouseRelease",clickReleaseHandler);

local function renderHandler()
{
    waitFPS = getFpsRate().tofloat()/28.0;
    if(frame > limit)
    {
        limit = frame + waitFPS;
        callEvent("ControllerInit.onFps");
    }
    ++frame;    
}

addEventHandler("onRender", renderHandler);
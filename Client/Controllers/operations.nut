
/**
Author: Quarchodron (http://gothic-online.com.pl/forum/member.php?action=profile&uid=15)
CC ( 2019 )
**/

/**
Load to creator all controllers menu.
**/

function renderControllers()
{	
	local arg = {};
	
	arg.init <- ControllerInit.Menu("Kreator",3000, 3500, 200, 0, 0);
	arg.init.addButton(0, 2000, 300, "Otw�rz projekt"); 
	arg.init.addButton(1, 2000, 300, "Zapisz projekt"); 	
	arg.init.addButton(2, 2000, 300, "Zako�cz");

	arg.manage <- ControllerInit.Menu("Menad�er",500, 500, 0, 190, 150);
	arg.manage.addButton(0, 2000, 200, "Nowy draw"); 
	arg.manage.addButton(1, 2000, 200, "Nowa tekstura"); 	
	
	arg.list <- ControllerInit.Menu("Elementy",3000, 500, 0, 190, 160);
	
	arg.element <- ControllerInit.Menu("Operacje",5500, 500, 128, 0, 230);
	arg.element.addButton(0, 2000, 200, "Usu�"); 	
	
	GUICreator.Controllers = arg; 
}

addEventHandler("onInit", renderControllers);

/**
When player click controller button.
**/

local function addClickControllerHandler(menu, id)
{
	if(menu == GUICreator.Controllers.init)
	{
		switch(id)
		{
			case 0: 
			GUICreator.Controllers.init.popDown();
			GUICreator.Controllers.manage.show();
			GUICreator.Controllers.list.show();
			GUICreator.openProject();
			break;
			case 1: GUICreator.saveProject(); break;
			case 2: GUICreator.close(); break;
		}
	}
	else if(menu == GUICreator.Controllers.manage)
	{
		switch(id)
		{
			case 0: GUICreator.addNewElement(1); break;
			case 1: GUICreator.addNewElement(2); break;
			case 2: break;
		}
	}	
	else if(menu == GUICreator.Controllers.list)
	{
		local aElementaElement = GUICreator.Elements[id];
		
		GUICreator.activeElement = id;
		
		reloadOperationsList();
		reloadElementPreview();
		GUICreator.Elements[id].ping();
	}	
	else if(menu == GUICreator.Controllers.element)
	{
		switch(id)
		{
			case 0: GUICreator.deleteElement(GUICreator.activeElement); break;
		}
	}		
}

addEventHandler("ControllerInit.onControllerClick", addClickControllerHandler);

/**
Key handler / Creator open.
**/

local function addHandlerKey(key)
{
	if(key == KEY_F12)
	{
		if(GUICreator.isOpen == false) GUICreator.open();
	}
}

addEventHandler("onKey", addHandlerKey)

/**
Function to operations menu
/ reloadOperationsList - for reload list of elements.
/ reloadElementPreview - reload preview for specific element.
**/

function reloadOperationsList()
{
	GUICreator.Controllers.list.resetButtons();
	
	foreach(v,elements in GUICreator.Elements)
	{
		GUICreator.Controllers.list.addButton(v, 2000, 200, elements.getInfo());
	}
}

function reloadElementPreview()
{
	if(GUICreator.Elements.len() > 0)
	{
		local aElementaElement = GUICreator.Elements[GUICreator.activeElement];
		GUICreator.Controllers.element.changeExtendedElement(aElementaElement);
		GUICreator.Controllers.element.show();
		return true;
	}
	GUICreator.Controllers.element.hide();
	return false;
}
class Kurde {

    constructor(cx) {
        x = cx;

		local element = this;
        addEventHandler("onRender", function(){ element.rendering() });	
    }

    function rendering(){
    }

    x = null;
}

addEventHandler("onInit", function(){
    teste <- Kurde(200);
});


enum io_type
{
	LINE,
	ALL
};

CreatorLib <- {
	packetReadString = 290,
	packetString = 291,	
	packedEndString = 292,
	
	packetReadTemplate = 293,
	packetTemplate = 294,
	packetEndTemplate = 295,
	
	packetStringTab = [],
	packetTemplateTab = [],	
}

function CreatorLib::packetHandler(pid, packet)
{
	local id = packet.readUInt16(); 

	switch(id)
	{
		case CreatorLib.packedEndString:
			CreatorLib.saveString();
		break;		
		case CreatorLib.packetReadString:
			CreatorLib.packetStringTab.clear();
		break;	
		case CreatorLib.packetString:
			CreatorLib.packetStringTab.append(packet.readString());
		break;
		case CreatorLib.packetEndTemplate:
			CreatorLib.saveTemplate();
		break;		
		case CreatorLib.packetReadTemplate:
			CreatorLib.packetStringTab.clear();
		break;	
		case CreatorLib.packetTemplate:
			CreatorLib.packetTemplateTab.append(packet.readString());
		break;		
	}
}

addEventHandler("onPacket", CreatorLib.packetHandler);

function CreatorLib::saveString()
{
    local resultFile = io.file("gui-creator/resultCreator.nut", "w");
	foreach(val in CreatorLib.packetStringTab)
	{
		resultFile.write(val+" \n");	
	}
	resultFile.close();
}

function CreatorLib::saveTemplate()
{
    local resultFile = io.file("gui-creator/templateCreator.nut", "w");
	foreach(val in CreatorLib.packetTemplateTab)
	{
		resultFile.write(val+" \n");	
	}
	resultFile.close();
}

function CreatorLib::sendTemplateToPlayer(pid)
{
    local loadTemplate = io.file("gui-creator/templateCreator.nut", "r");
    if (loadTemplate.isOpen)
    {
		local args = 0;
		do{
			args = loadTemplate.read(io_type.LINE);		
			local packet = Packet();
			packet.writeUInt16(CreatorLib.packetTemplate);
			packet.writeString(args);
			packet.send(pid, RELIABLE_ORDERED);
		} while (args != null)     
	}
	loadTemplate.close();
}

addEventHandler("onPlayerJoin",CreatorLib.sendTemplateToPlayer);
